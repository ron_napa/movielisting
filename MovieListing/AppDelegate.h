//
//  AppDelegate.h
//  MovieListing
//
//  Created by Ron Napa on 10/25/15.
//  Copyright (c) 2015 Ron Napa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

