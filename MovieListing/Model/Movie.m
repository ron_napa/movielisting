//
//  Movie.m
//  MovieListing
//
//  Created by Ron Napa on 10/25/15.
//  Copyright (c) 2015 Ron Napa. All rights reserved.
//

#import "Movie.h"

@implementation Movie

@synthesize rating;
@synthesize genres;
@synthesize language;
@synthesize title;
@synthesize url;
@synthesize title_long;
@synthesize imdb_code;
@synthesize movieId;
@synthesize state;
@synthesize year;
@synthesize runtime;
@synthesize overview;
@synthesize slug;
@synthesize mpa_rating;

@end
