//
//  Movie.h
//  MovieListing
//
//  Created by Ron Napa on 10/25/15.
//  Copyright (c) 2015 Ron Napa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Movie : NSObject

@property float rating;
@property (strong , nonatomic ) NSDictionary* genres;
@property (copy , nonatomic ) NSString* language;
@property (copy , nonatomic ) NSString* title;
@property (copy , nonatomic ) NSString* url;
@property (copy , nonatomic ) NSString* title_long;
@property (copy , nonatomic ) NSString* imdb_code;
@property int movieId;
@property (copy , nonatomic ) NSString* state;
@property int year;
@property int runtime;
@property (copy , nonatomic ) NSString* overview;
@property (copy , nonatomic ) NSString* slug;
@property (copy , nonatomic ) NSString* mpa_rating;

@end
