//
//  ViewController.h
//  MovieListing
//
//  Created by Ron Napa on 10/25/15.
//  Copyright (c) 2015 Ron Napa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movie.h"
#import "MovieDetailViewController.h"
#import "LandScapeViewController.h"

@interface ViewController : UIViewController

@property (strong,nonatomic) NSMutableArray *movieList;

@property (retain, nonatomic) IBOutlet UITableView *movieListTableView;

@end

