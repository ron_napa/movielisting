//
//  MovieDetailViewController.h
//  MovieListing
//
//  Created by Ron Napa on 10/26/15.
//  Copyright (c) 2015 Ron Napa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movie.h"

@interface MovieDetailViewController : UIViewController

@property (strong , nonatomic) Movie *currentMovie;

@property (retain, nonatomic) IBOutlet UILabel *movieTitleLabel;
@property (retain, nonatomic) IBOutlet UILabel *movieYearLabel;
@property (retain, nonatomic) IBOutlet UILabel *movieRatingsLabel;
@property (retain, nonatomic) IBOutlet UIImageView *movieCoverImageView;
@property (retain, nonatomic) IBOutlet UIImageView *movieBackdropImageView;
@property (retain, nonatomic) IBOutlet UITextView *movieOverviewTextView;

@end
