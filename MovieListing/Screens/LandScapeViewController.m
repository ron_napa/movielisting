//
//  LandScapeViewController.m
//  MovieListing
//
//  Created by Ron Napa on 10/26/15.
//  Copyright (c) 2015 Ron Napa. All rights reserved.
//

#import "LandScapeViewController.h"

@interface LandScapeViewController ()
@property (nonatomic, readwrite) UIStatusBarStyle oldStatusBarStyle;
@end

@implementation LandScapeViewController
@synthesize movieList;
@synthesize currentMovie;

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];

    [self displaySelectedMovie];
    
    if ([self respondsToSelector:@selector(preferredStatusBarStyle)] == NO){
        
        self.oldStatusBarStyle = [[UIApplication sharedApplication] statusBarStyle];
        
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackTranslucent animated:NO];
        
    }
    
}

- (void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    // On iOS 6, you must manually restore the status bar style.
    if ([self respondsToSelector:@selector(preferredStatusBarStyle)] == NO){
        
        [[UIApplication sharedApplication] setStatusBarStyle:self.oldStatusBarStyle animated:NO];
        
    }
    
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    
    return UIStatusBarStyleLightContent;
    
}

#pragma mark Rotation

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}

#pragma mark - public

- (void)displaySelectedMovie{
    
    [self.movieTitleLabel setText:[currentMovie title]];
    [self.movieYearLabel setText:[NSString stringWithFormat:@"%i",[currentMovie year]]];
    [self.movieRatingsLabel setText:[NSString stringWithFormat:@"%.01f",[currentMovie rating]]];
    [self.movieOverviewTextView setText:[currentMovie overview]];
    
    NSString *backDropUrlString = [NSString stringWithFormat:@"https://dl.dropboxusercontent.com/u/5624850/movielist/images/%@-backdrop.jpg" , [currentMovie slug]];
    NSURL *backDropUrl = [NSURL URLWithString:backDropUrlString];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        NSData *imageData = [NSData dataWithContentsOfURL:backDropUrl];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            UIImage *tempImage = [[UIImage alloc] initWithData:imageData];
            [self.movieBackdropImageView setImage:tempImage];
            
        });
        
    });
    
    NSString *coverUrlString = [NSString stringWithFormat:@"https://dl.dropboxusercontent.com/u/5624850/movielist/images/%@-cover.jpg" , [currentMovie slug]];
    NSURL *coverDropUrl = [NSURL URLWithString:coverUrlString];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        NSData *imageData = [NSData dataWithContentsOfURL:coverDropUrl];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            UIImage *tempImage = [[UIImage alloc] initWithData:imageData];
            [self.movieCoverImageView setImage:tempImage];
            
        });
        
    });
    
}

#pragma mark - UITableViewDelegate


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [movieList count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"movieCell"];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"movieCell"];
        
    }
    
    Movie *movieTemp = (Movie*)[movieList objectAtIndex:indexPath.row];
    
    UIImageView *movieImage = (UIImageView*)[cell viewWithTag:3];
    NSString *imageUrlString = [NSString stringWithFormat:@"https://dl.dropboxusercontent.com/u/5624850/movielist/images/%@-backdrop.jpg" , [movieTemp slug]];
    NSURL *imageUrl = [NSURL URLWithString:imageUrlString];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        NSData *imageData = [NSData dataWithContentsOfURL:imageUrl];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            UIImage *tempImage = [[UIImage alloc] initWithData:imageData];
            [movieImage setImage:tempImage];
            
        });
        
    });
    
    UILabel *movieTitle = (UILabel*)[cell viewWithTag:1];
    [movieTitle setText:[movieTemp title]];
    
    UILabel *movieYearRelease = (UILabel*)[cell viewWithTag:2];
    [movieYearRelease setText:[NSString stringWithFormat:@"%i",[movieTemp year]]];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewAutomaticDimension;
    
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewAutomaticDimension;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int movieID = indexPath.item;
    
    currentMovie = (Movie*)[movieList objectAtIndex:movieID];
    
    [self displaySelectedMovie];
    
}

@end
