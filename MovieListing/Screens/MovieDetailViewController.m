//
//  MovieDetailViewController.m
//  MovieListing
//
//  Created by Ron Napa on 10/26/15.
//  Copyright (c) 2015 Ron Napa. All rights reserved.
//

#import "MovieDetailViewController.h"

@interface MovieDetailViewController ()

@end

@implementation MovieDetailViewController
@synthesize currentMovie;
@synthesize movieTitleLabel;
@synthesize movieYearLabel;
@synthesize movieRatingsLabel;
@synthesize movieCoverImageView;
@synthesize movieBackdropImageView;
@synthesize movieOverviewTextView;


#pragma mark - LifeCycle 


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [movieTitleLabel setText:[currentMovie title]];
    [movieYearLabel setText:[NSString stringWithFormat:@"%i",[currentMovie year]]];
    [movieRatingsLabel setText:[NSString stringWithFormat:@"%.01f",[currentMovie rating]]];
    [movieOverviewTextView setText:[currentMovie overview]];
    
    NSString *backDropUrlString = [NSString stringWithFormat:@"https://dl.dropboxusercontent.com/u/5624850/movielist/images/%@-backdrop.jpg" , [currentMovie slug]];
    NSURL *backDropUrl = [NSURL URLWithString:backDropUrlString];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        NSData *imageData = [NSData dataWithContentsOfURL:backDropUrl];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            UIImage *tempImage = [[UIImage alloc] initWithData:imageData];
            [movieBackdropImageView setImage:tempImage];
            
        });
        
    });
    
    NSString *coverUrlString = [NSString stringWithFormat:@"https://dl.dropboxusercontent.com/u/5624850/movielist/images/%@-cover.jpg" , [currentMovie slug]];
    NSURL *coverDropUrl = [NSURL URLWithString:coverUrlString];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        NSData *imageData = [NSData dataWithContentsOfURL:coverDropUrl];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            UIImage *tempImage = [[UIImage alloc] initWithData:imageData];
            [movieCoverImageView setImage:tempImage];
            
        });
        
    });

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [movieTitleLabel release];
    [movieYearLabel release];
    [movieRatingsLabel release];
    [movieCoverImageView release];
    [movieBackdropImageView release];
    [movieOverviewTextView release];
    [super dealloc];
}

@end
