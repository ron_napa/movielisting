//
//  ViewController.m
//  MovieListing
//
//  Created by Ron Napa on 10/25/15.
//  Copyright (c) 2015 Ron Napa. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize movieList;


#pragma mark - Lifecycle


- (void)dealloc {
    [_movieListTableView release];
    [super dealloc];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onDeviceOrientationDidChange:)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
    
    movieList = [[NSMutableArray alloc] init];
    
    [self getMovieList];
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - public


- (void)getMovieList{
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Getting Movie List"
                                                        message:@"\n"
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:nil];
    
    [alertView show];
    
    NSString *requestUrl = [NSString stringWithFormat:@"https://dl.dropboxusercontent.com/u/5624850/movielist/list_movies_page1.json"];
    NSLog(requestUrl);
    
    NSMutableURLRequest *request = [NSMutableURLRequest
                                    requestWithURL:[NSURL URLWithString:requestUrl]];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"GET"];
    
    NSMutableData *xmlData = [[NSMutableData alloc] init];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc]init];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                               
                               if (error) {
                                   
                                   __block xmlData = nil;
                                   NSLog(@"error:%@", error.localizedDescription);
                                   [alertView dismissWithClickedButtonIndex:0 animated:YES];
                                   
                                   [self getMovieList];
                                   
                                   return;
                                   
                               }
                               
                               dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                                   
                                   dispatch_sync(dispatch_get_main_queue(), ^{
                                       
                                       [alertView dismissWithClickedButtonIndex:0 animated:YES];
                                       
                                   });
                                   
                               });
                               
                               [xmlData appendData:data];
                               NSString *someString = [[NSString alloc] initWithData:xmlData encoding:NSASCIIStringEncoding];
                               NSLog(@"Movies %@",someString);
                               [self holdAllMovies:someString];
                               
                           }];
    
}

- (void)holdAllMovies:(NSString*)result{
    
    NSLog(@"Handled List %@ ",result);
    NSError *err = nil;
    NSDictionary *array = [NSJSONSerialization JSONObjectWithData:[result dataUsingEncoding:NSUTF8StringEncoding]
                                                          options:NSJSONReadingMutableContainers
                                                            error:&err];
    NSDictionary *data = [array objectForKey:@"data"];
    
    for (NSDictionary *movieDictionary in [data objectForKey:@"movies"]) {
        
        Movie *movieNode = [[Movie alloc] init];
        [movieNode setRating:[[movieDictionary objectForKey:@"rating"] floatValue]];
        [movieNode setLanguage:[movieDictionary objectForKey:@"language"]];
        [movieNode setTitle:[movieDictionary objectForKey:@"title"]];
        [movieNode setUrl:[movieDictionary objectForKey:@"url"]];
        [movieNode setTitle_long:[movieDictionary objectForKey:@"title_long"]];
        [movieNode setImdb_code:[movieDictionary objectForKey:@"imdb_code"]];
        [movieNode setMovieId:[[movieDictionary objectForKey:@"movieId"] integerValue]];
        [movieNode setState:[movieDictionary objectForKey:@"state"]];
        [movieNode setYear:[[movieDictionary objectForKey:@"year"] integerValue]];
        [movieNode setRuntime:[[movieDictionary objectForKey:@"runtime"] integerValue]];
        [movieNode setOverview:[movieDictionary objectForKey:@"overview"]];
        [movieNode setSlug:[movieDictionary objectForKey:@"slug"]];
        [movieNode setMpa_rating:[movieDictionary objectForKey:@"mpa_rating"]];
        
        NSDictionary *movieGenres = [movieDictionary objectForKey:@"genres"];
        [movieNode setGenres:movieGenres];

        [movieList addObject:movieNode];
        
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0),
                   ^{
                       dispatch_async(dispatch_get_main_queue(),
                                      ^{
                                          
                                          [self.movieListTableView reloadData];
                                          
                                      });});
    
}


#pragma mark - UITableViewDelegate


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [movieList count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"movieCell"];
        
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"movieCell"];
        
    }
    
    Movie *movieTemp = (Movie*)[movieList objectAtIndex:indexPath.row];
    
    UIImageView *movieImage = (UIImageView*)[cell viewWithTag:3];
    NSString *imageUrlString = [NSString stringWithFormat:@"https://dl.dropboxusercontent.com/u/5624850/movielist/images/%@-backdrop.jpg" , [movieTemp slug]];
    NSURL *imageUrl = [NSURL URLWithString:imageUrlString];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        NSData *imageData = [NSData dataWithContentsOfURL:imageUrl];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            UIImage *tempImage = [[UIImage alloc] initWithData:imageData];
            [movieImage setImage:tempImage];
            
        });
        
    });

    UILabel *movieTitle = (UILabel*)[cell viewWithTag:1];
    [movieTitle setText:[movieTemp title]];
    
    UILabel *movieYearRelease = (UILabel*)[cell viewWithTag:2];
    [movieYearRelease setText:[NSString stringWithFormat:@"%i",[movieTemp year]]];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewAutomaticDimension;
    
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewAutomaticDimension;
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    NSLog(@"selected item");
    
    if ([segue.identifier isEqualToString:@"movieDetail"]) {
        
        NSIndexPath *indexPath = indexPath = [self.movieListTableView indexPathForSelectedRow];
        Movie *movieTemp = (Movie*)[movieList objectAtIndex:(int)indexPath.row];
        MovieDetailViewController *movieDetailViewController = segue.destinationViewController;
        movieDetailViewController.currentMovie = movieTemp;
        
    }
    
    if ([segue.identifier isEqualToString:@"PresentLandscapeViewControllerSegue"]) {
        
        NSLog(@"will rotate");
        NSIndexPath *indexPath = indexPath = [self.movieListTableView indexPathForSelectedRow];
        Movie *movieTemp = (Movie*)[movieList objectAtIndex:(int)indexPath.row];
        LandScapeViewController *landscapeView = segue.destinationViewController;
        landscapeView.currentMovie = movieTemp;
        landscapeView.movieList = movieList;
        
    }
    
}

- (void)onDeviceOrientationDidChange:(NSNotification *)notification{

    [self performSelector:@selector(updateLandscapeView) withObject:nil afterDelay:0];
    
}

- (void)updateLandscapeView{
    
    UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
    
    if (UIDeviceOrientationIsLandscape(deviceOrientation) && self.presentedViewController == nil){

        [self performSegueWithIdentifier:@"PresentLandscapeViewControllerSegue" sender:self];
        
    }else if (deviceOrientation == UIDeviceOrientationPortrait && self.presentedViewController != nil){
        
        [self dismissViewControllerAnimated:YES completion:NULL];
        
    }
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
    
}
@end
